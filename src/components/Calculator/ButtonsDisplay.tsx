import React from 'react';

const buttonTags = [
    'clear',
    '7', '8', '9',
    '4', '5', '6',
    '1', '2', '3',
    '=', '0',
];

const operatorTags = [
    '/', '*', '+', '-', '.'
]
type Props = {
    clickHandler: (button: string) => void
}
const ButtonsDisplay = (props: Props) => {

    const buttons = buttonTags.map((button) => {
        if (button === '=') {
            return <button key={button} id="equality" onClick={() => props.clickHandler(button)}>{button}</button>
        } else {
            return <button key={button} id={button} onClick={() => props.clickHandler(button)}>{button}</button>
        }
    });

    const operators = operatorTags.map((operator) => {
        return <button key={operator} id="operators" onClick={() => props.clickHandler(operator)}>{operator}</button>
    });

    return (
        <div className="button-display">
            <div className="numbers">
                {buttons}
            </div>
            <div className="operators">
                {operators}
            </div>
        </div>
    );
};

export default ButtonsDisplay;