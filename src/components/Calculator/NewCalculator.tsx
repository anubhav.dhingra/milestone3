import React, { useState } from 'react';
import Display from './Display';
import ButtonsDisplay from './ButtonsDisplay';

const NewCalculator = () => {

    const [calculationResult, setCalculationResult] = useState('' as string);
    const [calculationInputs, setCalculationInputs] = useState([] as string[]);
    const [stack, setStack] = useState([] as string[]);
    const [prevOperation, setPrevOperation] = useState('operator');

    const calculateResultValue = (clickedButton) => {
        if (prevOperation !== 'number') {
            setCalculationResult('ERROR! PLEASE START OVER.');
            return;
        }
        let inputValues:string[] = calculationInputs;
        inputValues.push(calculationResult);
        let result;
        if (clickedButton === '=') {
            result = eval(calculationInputs.join(' '));
            setCalculationInputs([]);
            setCalculationResult(result.toString());
            return;
        }
        if (inputValues.length > 1) {
            result = eval(calculationInputs.join(' '));
            inputValues = [result.toString()];
            setCalculationResult(result.toString());
        }
        inputValues.push(clickedButton);
        setStack([]);
        setCalculationInputs(inputValues);
        setPrevOperation('operator');
    }

    const registerInputNumber = (clickedButton) => {
        let currentStack = stack;
        currentStack.push(clickedButton);
        let value = currentStack.reduce((x, y) => x + y);
        setStack(currentStack);
        setCalculationResult(value);
        setPrevOperation('number');
    }

    const handleClick = (type) => {        
        switch (type) {
            case 'clear':
                setCalculationResult('0');
                setCalculationInputs([]); 
                setStack([]);
                setPrevOperation('');
                break;
            case '+':
                calculateResultValue('+');
                break;
            case '-':
                calculateResultValue('-');
                break;
            case '*':
                calculateResultValue('*');
                break;
            case '/':
                calculateResultValue('/');
                break;
            case '=':
                calculateResultValue('=');
                break;
            default:
                registerInputNumber(type);
                break;
        };
    }
    
    return (
        <div className="clculator-container">
            <Display display={calculationResult}/>
            <ButtonsDisplay clickHandler={handleClick}/>
        </div>
    );
}

export default NewCalculator;