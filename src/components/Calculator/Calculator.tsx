import React, { useEffect, useState } from 'react';
import Display from './Display';
import ButtonsDisplay from './ButtonsDisplay';
// import CalculatorContext from './CalculatorContext';

const Calculator = () => {

    // const appendDigit = useContext(CalculatorContext);

    const [display, setDisplay] = useState('');

    const [prevValue, setPrevValue] = useState('');

    const [calculatedValue, setCalculatedValue] = useState(true);

    const clickHandler = (type: string) => {
        switch (type) {
            case '+':
                operatorHandler(type, true);
                break;
            case '-':
                operatorHandler(type, true);
                break;
            case '*':
                operatorHandler(type);
                break;
            case '/':
                operatorHandler(type);
                break;
            case '=':
                showResult();
                break;
            default:
                digitHandler(type);
        }
    };

    const showResult = () => {
        let calculatedValue = eval(prevValue.concat(display)).toString();
        setDisplay(calculatedValue);
        setCalculatedValue(true);
    }

    const digitHandler = (type: string) => {
        if (display === '') {
            setDisplay(type);
        } else if (prevValue && calculatedValue) {
            setDisplay(type);
            setCalculatedValue(false);
        } else {
            setDisplay(display.concat(type));
        }
    };

    const operatorHandler = (type:string, check?: boolean) => {
        console.log('type',type,'display',display, 'prev',prevValue);
        
        if (check && display === '') {
            console.log('1');
            digitHandler(type);
        } else if (prevValue) {
            console.log('2');
            let calculatedValue = eval(prevValue.concat(display)).toString();
            setDisplay(calculatedValue);
            setCalculatedValue(true);
            setPrevValue(calculatedValue.concat(type));
        } else {
            console.log('3');
            setPrevValue(()=>display.concat(type));
            setDisplay('');
        }
    };

    // const calculations = (input: string) => {

    // }
    
    return (
        <React.Fragment>
            <Display display={display} />
            <ButtonsDisplay clickHandler={clickHandler} />
        </React.Fragment>
    );
};

export default Calculator;