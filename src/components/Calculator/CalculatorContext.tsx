import React, { createContext } from 'react';

interface AppContextInterface {
    appendDigit: () => void
}

export const CalculatorContext = createContext<AppContextInterface | null>(null);

const CalculatorContextProvider = () => {

    const appendDigit = () => {
        console.log('funcky');
    }

    return (
        <CalculatorContext.Provider value={{appendDigit}}>

        </CalculatorContext.Provider>
    )
}

export default CalculatorContextProvider;