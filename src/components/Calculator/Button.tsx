import React from 'react';

type Props = {
    type: string,
    clickHandler: (type: string) => void
}

const Button = (props: Props) => {
    return (
        <button onClick={() => props.clickHandler(props.type)}>{props.type}</button>
    );
};

export default Button;