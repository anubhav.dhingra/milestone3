import React from 'react';
import './Calculator.css';

type Props = {
    display: string
}

const Display = (props: Props) => {
    
    return (
        <div className="display-container">
            {props.display}
        </div>
    )
}

export default Display;