import React from 'react';
import {ProductType} from './ShoppingCart';
import CartItem from './CartItem';

type Props = {
    cartItems: ProductType[],
    addToCart: (addedItem: ProductType) => void
    removeFromCart: (id: number) => void
}

const Cart:React.FC<Props> = ({cartItems, addToCart, removeFromCart}) => {

    const calculateTotal = (products: ProductType[]) => {
        return products.reduce((acc: number, product) => acc + (product.price * product.quantity),0);
    }

    return (
        <div>
            <h2>Your Cart</h2>
            {
                cartItems?.map((item) => {
                    return <CartItem key={item.id} product={item} addToCart={addToCart} removeFromCart={removeFromCart}/>
                })
            }
            <h2>Total ${calculateTotal(cartItems).toFixed(2)}</h2>
        </div>
    )
};

export default Cart;