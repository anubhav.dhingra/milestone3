import React, { useState, useEffect } from 'react';
import Product from './Product';
import './ShoppingCart.css';
import SearchBar from './SearchBar';
import Cart from './Cart';
import axios from 'axios';

export type ProductType = {
    id: number,
    title: string,
    price: number,
    category: string,
    description: string,
    image: string,
    quantity: number
}

const ShoppingCart = () => {

    const [products, setProducts]: any = useState([]);

    const [cartItems, setCartItems]: any = useState([] as ProductType[]);

    const handleAddToCart = (addedItem: ProductType) => {
        setCartItems(prev => {
            const itemInCart = prev.find(item => item.id === addedItem.id);
            if (itemInCart) {
                return prev.map((item) => (
                    item.id === addedItem.id ? { ...item, quantity: item.quantity + 1 } : item
                ));
            }
            return [...prev, { ...addedItem, quantity: 1 }];
        });
    }

    const handleRemoveFromCart = (id: number) => {
        setCartItems(prev => (
            prev.reduce((acc, item) => {
                if (item.id === id) {
                    if (item.quantity === 1) {
                        return [...acc];
                    }
                    return [...acc, { ...item, quantity: item.quantity - 1 }]
                } else {
                    return [...acc, item];
                }
            }, [] as ProductType[])
        ))
    }

    const handleOnSelect = (item) => {
        console.log(item);
    };
    const handleOnSearch = (string, results) => {
        console.log(string,'m', results);
        if (string === '') {
            console.log('empty string');
            
        }
    };

    useEffect(() => {
        const fetchData = async () => {
            const result = await axios.get('https://fakestoreapi.com/products');
            setProducts(result.data);
        };
        fetchData();
    }, []);

    return (
        <div>
            <SearchBar products={products} handleOnSearch={handleOnSearch} handleOnSelect={handleOnSelect}/>
            <div className="container">
                <div className="product-listing">
                    {products.map((product) => {
                        return <Product product={product} key={product.id} handleAddToCart={handleAddToCart}></Product>
                    })}
                    {

                    }
                </div>
                <div className="cart-listing">
                    {
                        cartItems.length === 0 ? <img style={{maxHeight: "400px"}} alt="Empty Cart" src="https://i.pinimg.com/originals/2e/ac/fa/2eacfa305d7715bdcd86bb4956209038.png"></img> :
                        <Cart cartItems={cartItems} addToCart={handleAddToCart} removeFromCart={handleRemoveFromCart} />
                    }
                </div>
            </div>
        </div>
    )
};

export default ShoppingCart;