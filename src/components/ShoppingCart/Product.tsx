import React from 'react';

const Product = (props) => {
    
    return (
        <div className="product-card">
            <img src={props.product.image} alt={props.product.title}></img>
            <div style={{display: "contents", fontSize: "small"}}>
                <h3>{props.product.title}</h3>
                <p>{props.product.description}</p>
                <h3>${props.product.price}</h3>
            </div>

            <button onClick={() => props.handleAddToCart(props.product)}>Add to cart</button>
        </div>
    )
};

export default Product;