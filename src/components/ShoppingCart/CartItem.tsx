import React from 'react';
import { ProductType } from './ShoppingCart';

type Props = {
    product: ProductType;
    addToCart: (addedItem: ProductType) => void;
    removeFromCart: (id: number) => void;
};

const CartItem: React.FC<Props> = ({ product, addToCart, removeFromCart }) => {

    return (
        <React.Fragment>
            <h3>{product.title}</h3>
            <div className="cart-item-card">
                <div>
                    <p>Price ${product.price}</p>
                    <p>Total ${product.quantity * product.price}</p>

                    <div className="cart-buttons">
                        <button onClick={() => removeFromCart(product.id)}>-</button>
                        <p>{product.quantity}</p>
                        <button onClick={() => addToCart(product)}>+</button>
                    </div>
                </div>
                <img src={product.image} alt={product.title}></img>
            </div>
        </React.Fragment>
    )
};

export default CartItem;