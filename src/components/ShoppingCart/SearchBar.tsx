import React from 'react';
import { ReactSearchAutocomplete } from "react-search-autocomplete";


const SearchBar = (props) => {

    return (
        <div style={{width: "300px", margin:"auto 279px", marginTop: "20px"}}>
            <ReactSearchAutocomplete
                items={props.products}
                onSearch={props.handleOnSearch}
                onSelect={props.handleOnSelect}
                fuseOptions={{ keys: ["title", "description"], minMatchCharLength: 3}}
                resultStringKeyName="title"
                styling={{ zIndex: 2 }} // To display it on top of the search box below
                autoFocus
                placeholder="what are you looking for..."
            />
        </div>
    )

};

export default SearchBar;