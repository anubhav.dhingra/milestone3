import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => {
    return (
        <div className="header">
            <Link to="/counter">
                Counter
            </Link>
            <Link to="/rectangles">
                Rectangles
            </Link>
            <Link to="/calculator">
                Calculator
            </Link>
            <Link to="/shoppingCart">
                Shopping Cart
            </Link>
        </div>
    )
};

export default Header;