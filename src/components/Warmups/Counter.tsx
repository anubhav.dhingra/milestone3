import React, {useState} from 'react';
import './Warmups.css';

const Counter = () => {
    
    const [counter, setCounter] = useState(0);

    return (
        <div className="counter-container">
            {counter}
            <button className="subtract-counter" onClick={() => setCounter(counter-1)}>-</button>
            <button className="add-counter" onClick={() => setCounter(counter+1)}>+</button>
        </div>
    )
};

export default Counter;