import React from  'react';
import ImageComponent from './ImageComponent';
import Description from './Description';
import TitleComponent from './TitleComponent';
import './Warmups.css';

const RectangleComponent = () => {
    return (
        <div className="rectangle-container">
            <TitleComponent></TitleComponent>
            <ImageComponent></ImageComponent>
            <Description></Description>
        </div>
    );
};

export default RectangleComponent;