import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Counter from './components/Warmups/Counter';
import RectangleComponent from './components/Warmups/RectangleContainer';
import NewCalculator from './components/Calculator/NewCalculator';
import Header from './components/Header';
import ShoppingCart from './components/ShoppingCart/ShoppingCart';
import './App.css';

function App() {
  return (
    <React.Fragment>
      <Header />
      <div className="main">
        <Switch>
          <Route path="/rectangles">
            <RectangleComponent></RectangleComponent>
          </Route>
          <Route path="/calculator">
            <NewCalculator />
          </Route>
          <Route path="/shoppingCart">
            <ShoppingCart />
          </Route>
          <Route path="/">
            <Counter></Counter>
          </Route>
        </Switch>
      </div>
    </React.Fragment>
  );
}

export default App;
